# The.Swift.Dev.

Playground Book

## Subscribe

[The.Swift.Dev Playground Book](https://developer.apple.com/ul/sp0?url=https://gitlab.com/theswiftdev/playgrounds/raw/master/feed.json)

## External sources

* [Playground Subscriptions](https://developer.apple.com/swift-playgrounds/subscriptions/)
* [Playground Book Format Reference](https://developer.apple.com/library/archive/documentation/Xcode/Conceptual/swift_playgrounds_doc_format/index.html)

