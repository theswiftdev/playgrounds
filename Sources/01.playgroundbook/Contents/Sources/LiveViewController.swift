import PlaygroundSupport
import UIKit

public class LiveViewController: UIViewController, PlaygroundLiveViewMessageHandler, PlaygroundLiveViewSafeAreaContainer {

    override public func viewDidLoad() {
        super.viewDidLoad()

    }

    public func receive(_ message: PlaygroundValue) {
        guard case .data(let data) = message else {
            return
        }
    }
}

